export const settingList = [
  {
    key: 'system',
    name: '系统消息',
    component: 'base-setting',
  },
  {
    key: 'ding-talk',
    name: '钉钉消息',
    component: 'ding-talk-setting',
  },
  {
    key: 'email',
    name: '邮箱消息',
    component: 'email-setting',
  },
  {
    key: 'sms',
    name: '短信消息',
    component: 'sms-setting',
  },
];
