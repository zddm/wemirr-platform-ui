/**
 * @zh_CN GITHUB 仓库地址
 */
export const VBEN_GITHUB_URL = 'https://gitee.com/battcn/wemirr-platform';

/**
 * @zh_CN 文档地址
 */
export const VBEN_DOC_URL = 'https://docs.battcn.com';

/**
 * @zh_CN Vben Logo
 */
export const VBEN_LOGO_URL = 'https://docs.battcn.com/favicon.ico';

/**
 * @zh_CN WEMIRR-PLATFORM 首页地址
 */
export const VBEN_PREVIEW_URL = 'https://cloud.battcn.com';

export const VBEN_ELE_PREVIEW_URL = 'https://cloud.battcn.com';

export const VBEN_NAIVE_PREVIEW_URL = 'https://cloud.battcn.com';

export const VBEN_ANT_PREVIEW_URL = 'https://cloud.battcn.com';
